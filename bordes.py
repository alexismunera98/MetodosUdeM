﻿# -*- coding: utf-8 -*-
"""
Created on Wed Sep 12 16:27:15 2018

@author: s203e28
"""

import scipy as sp #librería de señales
import numpy as np #librería numérica
import matplotlib.pyplot as plt #librería de graficación
import scipy.ndimage as nd
import cv2
import sympy as sym
from sympy import * #agregamos la libraría simbólica para trabajar con variables
from sympy import Symbol
from scipy.misc import derivative
import math
import sympy as sp
from math import sin,cos,tan,asin,acos,atan,sqrt,log,exp
from math import sinh,cosh,tanh,asinh,acosh,atanh


epsilon = np.finfo(np.float32).eps
print(epsilon)
x = Symbol('x') #se declara la variable x como simbólica
fun = vars(math) #nos aseguramos que podamos utilizar funciones trigonométricas u otras funciones especiales
epsilon = np.finfo(np.float32).eps
imagen  = cv2.imread('leon.jpg',cv2.IMREAD_GRAYSCALE) # cargamos una imagen de muestra que se encuentra en la librería scipy
plt.imshow(imagen) #visualización de la imagen
plt.show()
LoG = nd.gaussian_laplace(imagen, 2) #aplicación del filtro Laplaciano de Gauss a la imagen de muestra
plt.imshow(LoG, cmap='gray') #visualizamos la imagen filtrada
plt.show()
print(LoG.shape)
vector1=[]
vector2=[] 
columna=0
x=[0,1,2,3,4,5,6]
auxiliar=0
imgBlack=np.zeros((626,1016,3),np.uint8)


def lagrange(aux,aux2):
    xi = np.array(tuple(aux))
    fi = np.array(tuple(aux2))

    # PROCEDIMIENTO
    n = len(xi)
    x = sym.Symbol('x')
    # Polinomio
    polinomio = 0
    for i in range(0,n):
        # Termino de Lagrange
        termino = 1
        for j  in range(0,n):
            if (j!=i):
                termino = termino*(x-xi[j])/(xi[i]-xi[j])
        polinomio = polinomio + termino*fi[i]
    # Expande el polinomio
    px = polinomio.expand()
    # para evaluacion numérica
    pxn = sym.lambdify(x,polinomio)
    return str(px)

def Biseccion(funcion,inicial,final,epsi):
    if abs(eval(funcion,fun,{'x':inicial})) <= epsi :
        raiz=inicial
        return raiz
    if abs(eval(funcion,fun,{'x':final})) <= epsi :
        raiz=final
        return raiz
    nuevo=(final+inicial)/2
    contador=0
    while abs(eval(funcion,fun,{'x':nuevo})) >= epsi and contador<40:
        if (eval(funcion,fun,{'x':nuevo}) * eval(funcion,fun,{'x':inicial})) > 0 :
            inicial=nuevo
        else:
            final=nuevo
        nuevo=(final+inicial)/2
        contador+=1
    raiz=nuevo
    return raiz

def Newton(funcion,inicial,epsi):
    derivada=sp.diff(funcion,x)
    if abs(eval(funcion,fun,{'x':inicial})) <= epsi :
        raiz=inicial
        return raiz
    nuevo=inicial-(eval(funcion,fun,{'x':inicial})/eval(str(derivada),fun,{'x':inicial}))
    while abs(eval(funcion,fun,{'x':nuevo})) >= epsi :
        inicial=nuevo
        nuevo=inicial-(eval(funcion,fun,{'x':inicial})/eval(str(derivada),fun,{'x':inicial}))
    raiz=nuevo
    return raiz    

auxiliar=0
for fila in range(0,626):
   print("fila",fila)
   for columna in range(0,619):   
        aux=LoG[fila,:]
        vector1=np.array(aux[columna:columna+7])
        #print("vector",vector1)
        auxiliar = Biseccion(lagrange(x,vector1),-50,50,epsilon)
        if(auxiliar != 0):
            imgBlack[fila,columna]=[255,255,255]
            print("true")
        else:
            print("false")

    
cv2.imshow("negro",imgBlack)
cv2.waitKey(0)
cv2.destroyAllWindows()        
        
        
        

